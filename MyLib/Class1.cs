﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Security.Cryptography;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.InteropServices;

namespace MyLib
{
    public class MyLib
    {
        const int Diff0 = 20;
        static Random r;
        static byte[] Gen = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };    // Generator seed for DH scheme

        static byte[] Rol(byte[] B)
        {
            int b = B.Length;
            byte[] C = new byte[b];

            for (int i = b - 1; i > 0; i--)
            {
                C[i - 1] += (byte)(B[i] << 7);
                C[i] += (byte)(B[i] >> 1);
            }
            C[0] += (byte)(B[0] >> 1);
            return C;
        }
        static byte[] Sub(byte[] A, byte[] B)
        {
            int a = A.Length;
            int b = B.Length;
            int[] C = new int[a];
            for (int i = 0; i < (a < b ? a : b); i++)
            {
                C[i] += A[i] - B[i];
                if (C[i] < 0)
                {
                    C[i + 1] = -1;
                    C[i] += 256;
                }
            }
            for (int i = b; i < a; i++)
                C[i] += A[i];
            for (int i = 0; i < a; i++)
                A[i] = (byte)C[i];
            return (A);
        }
        static byte[] E = { 0 };
        static int Cmpn(byte[] A, byte[] B)
        {
            int a = A.Length;
            int b = B.Length;
            if (b > a)
                return 0;
            if (b < a)
                return 1;
            for (int i = a - 1; i >= 0; i--)
                if (A[i] > B[i])
                    return 1;
                else
                    if (A[i] < B[i])
                    return 0;
            return 1;
        }
        static byte[] WRol(byte[] B, int x)
        {
            int b = B.Length;
            byte[] C = new byte[x];
            for (int i = 0; i < b; i++)
                C[x - i - 1] = B[b - i - 1];
            return C;
        }
        static byte[] Prn(byte[] A)
        {
            int i = A.Length - 1;
            byte[] C;
            if (i >= 0)
            {
                while (i > 0 & A[i] == 0) i--;
                C = new byte[i + 1];
                for (int j = 0; j <= i; j++)
                    C[j] = A[j];
                return C;
            }
            return E;
        }
        static byte[] Modn(byte[] A, byte[] B)
        {
            A = Prn(A);
            B = Prn(B);
            int a = A.Length;
            int b = B.Length;
            if (a < b)
                return A;
            B = WRol(B, a + 1);
            for (int i = 0; i <= ((a - b + 1) << 3); i++)
            {
                int s = Cmpn(A, B);
                if (s == 1)
                    A = Prn(Sub(A, B));
                B = Prn(Rol(B));
            }
            return Prn(A);
        }
        static byte[] Muln(byte[] A, byte[] B)
        {
            int a = A.Length;
            int b = B.Length;
            int c = a + b;
            int[] C = new int[c];
            for (int i = 0; i < a; i++)
                for (int j = 0; j < b; j++)
                {
                    int m = C[i + j] + A[i] * B[j];
                    C[i + j] = (byte)m;
                    C[i + j + 1] += (byte)(m >> 8);
                }
            byte[] Res = new byte[c];
            for (int i = 0; i < c; i++)
                Res[i] = (byte)C[i];
            return Res;
        }
        static byte[] Reverse(byte[] A)
        {
            byte[] B = new byte[A.Length];
            for (int i = 0; i < A.Length; i++)
                B[B.Length - 1 - i] = A[i];
            return B;
        }
        static byte[] Gornerpb(byte[] B, byte[] X, byte[] A)//Main Gorner
        {
            int a = X.Length;
            byte[] C = { 1 };
            for (int i = 0; i < a; i++)
            {
                int n = X[i];
                for (int j = 0; j < 8; j++)
                {
                    if (n % 2 != 0)
                        C = Prn(Modn(Muln(C, B), A));
                    n >>= 1;
                    B = Modn(Muln(B, B), A);
                }
            }
            return C;
        }
        [Serializable]
        public class Block
        {
            public enum block_type
            {
                BType_Init = 0,
                BType_ChPwd = 1,
                BType_ChKey = 2,
                BType_Ban = 3
            };
            public UInt64 number;
            public long timestamp;
            public byte[] prev_hash;   // 64 byte SHA-512 hash
            public int length;
            public block_type type;    // ?
            public byte[] id_hash { get; set; }     // SHA-512 of userId
            public byte[] key_public_N { get; set; }  // ? bytes
            public byte[] key_public_E { get; set; }
            public byte[] key_private { get; set; } // encrypted with symm algo
            public byte[] DH_Publickey { get; set; }
            public byte[] DH_Secretkey { get; set; }
            public byte difficulty;    // ?
            public byte[] nonce;       // 64 byte nonce
            public byte[] hash;
            public Block()
            {
                number = 0;
                timestamp = DateTime.Now.Ticks;
                prev_hash = new byte[1];
                type = 0;
                id_hash = new byte[64];
                RSACryptoServiceProvider RSAcsp = new RSACryptoServiceProvider();
                var key = RSAcsp.ExportParameters(true);
                key_public_N = key.Modulus;                                 // Common parameters for DH: Modulus
                key_public_E = Gornerpb(Gen, key.Exponent, key.Modulus);      // Generator
                key_private = new byte[1];
                DH_Publickey = new byte[1];
                DH_Secretkey = new byte[1];
                difficulty = Diff0;
                nonce = new byte[64];
                length = 3 * 64 + 12 + sizeof(block_type) + key_private.Length + key_public_N.Length + key_public_E.Length + DH_Secretkey.Length + DH_Publickey.Length;
                SHA512 s = new SHA512Managed();
                do      // for first block we are able to find collision locally
                {
                    fillNonce();
                    byte[] arr = toArray();
                    hash = s.ComputeHash(arr);
                } while (!checkDiff());
            }
            public Block(Block b, string userId, block_type t, string userPwd, byte[] DHM, byte[] DHG)
            {
                number = b.number + 1;
                timestamp = DateTime.Now.Ticks;
                prev_hash = b.hash;
                type = t;
                SHA512 s = new SHA512Managed();
                id_hash = s.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userId));
                byte[] pwd_hash = s.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userPwd));
                Aes aes = Aes.Create();
                RSACryptoServiceProvider RSAcsp = new RSACryptoServiceProvider();
                var key = RSAcsp.ExportParameters(true);
                byte[] aeskey = new byte[aes.KeySize / 8];
                byte[] aesiv = new byte[aes.BlockSize / 8];
                for (int i = 0; i < aeskey.Length; i++)
                    aeskey[i] = pwd_hash[i];
                for (int i = 0; i < aesiv.Length; i++)
                    aesiv[i] = pwd_hash[i + aeskey.Length];
                var encryptor = aes.CreateEncryptor(aeskey, aesiv);
                key_private = encryptor.TransformFinalBlock(key.D, 0, key.D.Length);
                key_public_E = key.Exponent;
                key_public_N = key.Modulus;
                difficulty = b.difficulty;
                byte[] tDH_Secretkey = new byte[64];
                r.NextBytes(tDH_Secretkey);
                DH_Publickey = Gornerpb(DHG, tDH_Secretkey, DHM);
                DH_Secretkey = encryptor.TransformFinalBlock(tDH_Secretkey, 0, tDH_Secretkey.Length);
                nonce = new byte[64];
                length = 3 * 64 + 12 + sizeof(block_type) + key_private.Length + key_public_N.Length + key_public_E.Length + DH_Secretkey.Length + DH_Publickey.Length;
                do
                {
                    fillNonce();
                    byte[] arr = toArray();
                    hash = s.ComputeHash(arr);
                } while (!checkDiff());
            }
            public bool checkHash()
            {
                if (difficulty == 0)
                    return false;
                if (!checkDiff())
                    return false;
                SHA512 s = new SHA512Managed();
                byte[] arr = toArray();
                byte[] hash_t = s.ComputeHash(arr);
                for (int i = 0; i < hash_t.Length; i++)
                    if (hash[i] != hash_t[i])
                        return false;
                return true;
            }
            private byte[] toArray()
            {
                byte[] arr = new byte[length + 8 /*sizeof(DateTime)*/ + 12/*sizeof(number&length)*/+ 64];
                arr[0] = (byte)number;
                arr[1] = (byte)(number >> 8);
                arr[2] = (byte)(number >> 16);
                arr[3] = (byte)(number >> 24);
                arr[4] = (byte)timestamp;
                arr[5] = (byte)(timestamp >> 8);
                arr[6] = (byte)(timestamp >> 16);
                arr[7] = (byte)(timestamp >> 24);
                arr[8] = (byte)(timestamp >> 32);
                arr[9] = (byte)(timestamp >> 40);
                arr[10] = (byte)(timestamp >> 48);
                arr[11] = (byte)(timestamp >> 56);
                arr[12] = (byte)type;
                int c = 13;
                for (int i = 0; i < 64; i++, c++)
                {
                    arr[c] = id_hash[i];
                }
                for (int i = 0; i < key_private.Length; i++)
                    arr[c++] = key_private[i];
                for (int i = 0; i < key_public_N.Length; i++)
                    arr[c++] = key_public_N[i];
                for (int i = 0; i < key_public_E.Length; i++)
                    arr[c++] = key_public_E[i];
                for (int i = 0; i < DH_Secretkey.Length; i++)
                    arr[c++] = DH_Secretkey[i];
                for (int i = 0; i < DH_Publickey.Length; i++)
                    arr[c++] = DH_Publickey[i];
                arr[c++] = difficulty;
                for (int i = 0; i < 64; i++, c++)
                    arr[c] = nonce[i];
                return arr;
            }
            private void fillNonce()
            {
                r.NextBytes(nonce);
            }
            private bool checkDiff()
            {
                for (int i = 0; i < difficulty; i++)
                    if (((hash[i >> 3] >> (i & 7)) & 1) == 1)
                        return false;
                return true;
            }
            public void Print()
            {
                Console.WriteLine("Block number: {0}", number);
                Console.WriteLine("Timestamp: {0:X16}", timestamp);
                Console.Write("Previous hash: ");
                for (int i = 0; i < prev_hash.Length; i++)
                    Console.Write("{0:X2}", prev_hash[i]);
                Console.WriteLine();
                Console.WriteLine("Data length: {0}", length);
                Console.Write("User Id hash: ");
                for (int i = 0; i < id_hash.Length; i++)
                    Console.Write("{0:X2}", id_hash[i]);
                Console.WriteLine();
                if (key_public_N != null)
                {
                    Console.Write("Public key Modulus: ");
                    for (int i = 0; i < key_public_N.Length; i++)
                        Console.Write("{0:X2}", key_public_N[i]);
                    Console.WriteLine();
                }
                if (key_public_E != null)
                {
                    Console.Write("Public key Exponent: ");
                    for (int i = 0; i < key_public_E.Length; i++)
                        Console.Write("{0:X2}", key_public_E[i]);
                    Console.WriteLine();
                }
                if (key_private != null)
                {
                    Console.Write("Private key: ");
                    for (int i = 0; i < key_private.Length; i++)
                        Console.Write("{0:X2}", key_private[i]);
                    Console.WriteLine();
                }
                if (DH_Publickey != null)
                {
                    Console.Write("DH public key: ");
                    for (int i = 0; i < DH_Publickey.Length; i++)
                        Console.Write("{0:X2}", DH_Publickey[i]);
                    Console.WriteLine();
                }
                if (DH_Secretkey != null)
                {
                    Console.Write("DH private key: ");
                    for (int i = 0; i < DH_Secretkey.Length; i++)
                        Console.Write("{0:X2}", DH_Secretkey[i]);
                    Console.WriteLine();
                }
                Console.WriteLine("Diff.: {0}", difficulty);
                if (nonce != null)
                {
                    Console.Write("Nonce: ");
                    for (int i = 0; i < nonce.Length; i++)
                        Console.Write("{0:X2}", nonce[i]);
                    Console.WriteLine();
                }
                if (hash != null)
                {
                    Console.Write("Hash: ");
                    for (int i = 0; i < hash.Length; i++)
                        Console.Write("{0:X2}", hash[i]);
                    Console.WriteLine();
                }
                bool checkblock = checkHash();
                if (checkblock)
                    Console.WriteLine("Hash is correct!");
                else
                    Console.WriteLine("Hash error!");
            }
        }
        public class User
        {
            public byte[] id_hash { get; set; }     // SHA-512 of userId
            public byte[] key_public_N { get; set; }  // ? bytes
            public byte[] key_public_E { get; set; }
            public byte[] key_private { get; set; } // encrypted with symm algo
            public byte[] DH_Publickey { get; set; }
            public byte[] DH_Secretkey { get; set; }
            public byte[] DHM;
            public byte[] DHG;
            public Chain c;
            public List<Connection> connections;
            public User(Block userBlock, string userPassword, Chain chain)
            {
                connections = new List<Connection>();
                id_hash = userBlock.id_hash;
                key_public_E = userBlock.key_public_E;
                key_public_N = userBlock.key_public_N;
                byte[] tmp = userBlock.key_private;
                SHA512 s = new SHA512Managed();
                byte[] pwd_hash = s.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userPassword));
                Aes aes = Aes.Create();
                byte[] aeskey = new byte[aes.KeySize / 8];
                byte[] aesiv = new byte[aes.BlockSize / 8];
                for (int i = 0; i < aeskey.Length; i++)
                    aeskey[i] = pwd_hash[i];
                for (int i = 0; i < aesiv.Length; i++)
                    aesiv[i] = pwd_hash[i + aeskey.Length];
                var decryptor = aes.CreateDecryptor(aeskey, aesiv);
                try
                {
                    key_private = decryptor.TransformFinalBlock(tmp, 0, tmp.Length);
                }
                catch
                {
                    Console.WriteLine("Invalid password");
                    return;
                }
                DH_Publickey = userBlock.DH_Publickey;
                tmp = userBlock.DH_Secretkey;
                DH_Secretkey = decryptor.TransformFinalBlock(tmp, 0, tmp.Length);
                c = chain;
                DHG = c.DHG;
                DHM = c.DHM;
            }
            public void test()
            {
                byte[] tDH_Public = Gornerpb(DHG, DH_Secretkey, DHM);
                if (tDH_Public.Length != DH_Publickey.Length)
                    Console.WriteLine("Wrong DH Secret or Public values!");
                else
                {
                    bool flag = true;
                    for (int i = 0; i < tDH_Public.Length; i++)
                        if (tDH_Public[i] != DH_Publickey[i])
                            flag = false;
                    if (flag)
                        Console.WriteLine("DH parameters OK");
                    else
                        Console.WriteLine("DH parameters error");
                }
            }
            public void Connect(string userId)
            {
                Block b = c.findUser(userId);
                if (b == null)
                {
                    Console.WriteLine("User not found");
                    return;
                }
                connections.Add(new Connection(c, b, DH_Secretkey));
                byte[] dk = connections[connections.Count - 1].DHKey;
                Console.WriteLine("DH Key MAC: {0:X16}", connections[connections.Count - 1].DHKeyMAC());
            }
        }
        [Serializable]
        public class Chain
        {
            public Block b0;
            public List<Block> blockChain;
            public byte[] DHM;  // common modulus for DH
            public byte[] DHG;  // common generator for DH
            public Chain()
            {
                if (r == null)
                    r = new Random();
                b0 = new Block();
                DHM = b0.key_public_N;
                DHG = b0.key_public_E;
            }
            public void AddBlock(Block b)
            {
                if (b0 == null)
                {
                    b0 = b;
                    return;
                }
                if (blockChain == null)
                {
                    blockChain = new List<Block>();
                }
                blockChain.Add(b);
            }
            public void AddBlock(string userId, Block.block_type t, string userPwd)
            {
                if (blockChain == null)
                {
                    blockChain = new List<Block>();
                    blockChain.Add(new Block(b0, userId, t, userPwd, DHM, DHG));
                }
                else
                    blockChain.Add(new Block(blockChain[blockChain.Count - 1], userId, t, userPwd, DHM, DHG));
            }
            public Block findUser(string userId)
            {
                SHA512 s = new SHA512Managed();
                byte[] id_hash = s.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userId));
                if (blockChain == null)
                    return null;
                foreach (Block b in blockChain)
                {
                    byte[] tmp = b.id_hash;
                    bool f = true;
                    for (int i = 0; i < tmp.Length && i < id_hash.Length; i++)
                        if (id_hash[i] != tmp[i])
                        {
                            f = false;
                            break;
                        }
                    if (f)
                        return b;
                }
                return null;
            }
            public void Print()
            {
                if (b0 != null)
                {
                    Console.WriteLine("Initial block:");
                    b0.Print();
                    if (blockChain != null)
                    {
                        Console.WriteLine("There are {0} blocks in chain.", blockChain.Count);
                        if (blockChain.Count > 0)
                            foreach (Block b in blockChain)
                                b.Print();
                    }
                }
                else
                    Console.WriteLine("No blocks in chain!");
            }
        }
        static Chain FromXmlFile(string fileName)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Chain));
            FileStream fs = new FileStream(fileName + ".xml", FileMode.Open);
            Chain c = (Chain)formatter.Deserialize(fs);
            fs.Close();
            c.DHM = c.b0.key_public_N;
            c.DHG = c.b0.key_public_E;
            return c;
        }
        static void RSATest()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            RSAParameters rsap = rsa.ExportParameters(true);
            Console.WriteLine("RSA Modulus:");
            for (int i = 0; i < rsap.Modulus.Length; i++)
                Console.Write("{0:X2}", rsap.Modulus[i]);
            Console.WriteLine();
            Console.WriteLine("RSA Exponent:");
            for (int i = 0; i < rsap.Exponent.Length; i++)
                Console.Write("{0:X2}", rsap.Exponent[i]);
            Console.WriteLine();
            Console.WriteLine("RSA D:");
            for (int i = 0; i < rsap.D.Length; i++)
                Console.Write("{0:X2}", rsap.D[i]);
            Console.WriteLine();
            Console.WriteLine("Clear text:");
            byte[] data = { 1, 2, 3, 4 };
            for (int i = 0; i < data.Length; i++)
                Console.Write("{0:X2}", data[i]);
            Console.WriteLine();
            byte[] wtf = Gornerpb(data, Reverse(rsap.Exponent), Reverse(rsap.Modulus));
            Console.WriteLine("Cipher text:");
            for (int i = 0; i < wtf.Length; i++)
                Console.Write("{0:X2}", wtf[i]);
            Console.WriteLine();
            Console.WriteLine("Decrypted text:");
            byte[] wtfd = Gornerpb(wtf, Reverse(rsap.D), Reverse(rsap.Modulus));
            for (int i = 0; i < wtfd.Length; i++)
                Console.Write("{0:X2}", wtfd[i]);
            Console.WriteLine();
        }
        static void UserKeyTest(Block userBlock)
        {
            byte[] M = userBlock.key_public_N;
            byte[] E = userBlock.key_public_E;
            byte[] D = userBlock.key_private;
            Console.WriteLine("RSA Modulus:");
            for (int i = 0; i < M.Length; i++)
                Console.Write("{0:X2}", M[i]);
            Console.WriteLine();
            Console.WriteLine("RSA Exponent:");
            for (int i = 0; i < E.Length; i++)
                Console.Write("{0:X2}", E[i]);
            Console.WriteLine();
            Console.WriteLine("RSA D:");
            for (int i = 0; i < D.Length; i++)
                Console.Write("{0:X2}", D[i]);
            Console.WriteLine();
            Console.WriteLine("Clear text:");
            byte[] data = { 1, 2, 3, 4 };
            for (int i = 0; i < data.Length; i++)
                Console.Write("{0:X2}", data[i]);
            Console.WriteLine();
            byte[] wtf = Gornerpb(data, Reverse(E), Reverse(M));
            Console.WriteLine("Cipher text:");
            for (int i = 0; i < wtf.Length; i++)
                Console.Write("{0:X2}", wtf[i]);
            Console.WriteLine();
            Console.WriteLine("Decrypted text:");
            byte[] wtfd = Gornerpb(wtf, Reverse(D), Reverse(M));
            for (int i = 0; i < wtfd.Length; i++)
                Console.Write("{0:X2}", wtfd[i]);
            Console.WriteLine();
        }
        static void UserKeyTestPwd(Block userBlock, string userPassword, byte[] DHG, byte[] DHM)
        {
            byte[] M = userBlock.key_public_N;
            byte[] E = userBlock.key_public_E;
            byte[] tD = userBlock.key_private;
            SHA512 s = new SHA512Managed();
            byte[] pwd_hash = s.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userPassword));
            Aes aes = Aes.Create();
            byte[] aeskey = new byte[aes.KeySize / 8];
            byte[] aesiv = new byte[aes.BlockSize / 8];
            for (int i = 0; i < aeskey.Length; i++)
                aeskey[i] = pwd_hash[i];
            for (int i = 0; i < aesiv.Length; i++)
                aesiv[i] = pwd_hash[i + aeskey.Length];
            var decryptor = aes.CreateDecryptor(aeskey, aesiv);
            byte[] D;
            byte[] DH_Secret;
            try
            {
                D = decryptor.TransformFinalBlock(tD, 0, tD.Length);
                DH_Secret = decryptor.TransformFinalBlock(userBlock.DH_Secretkey, 0, userBlock.DH_Secretkey.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured:");
                Console.WriteLine(e.Message);
                Console.WriteLine("Is password correct?");
                return;
            }
            Console.WriteLine("RSA Modulus:");
            for (int i = 0; i < M.Length; i++)
                Console.Write("{0:X2}", M[i]);
            Console.WriteLine();
            Console.WriteLine("RSA Exponent:");
            for (int i = 0; i < E.Length; i++)
                Console.Write("{0:X2}", E[i]);
            Console.WriteLine();
            Console.WriteLine("RSA D:");
            for (int i = 0; i < D.Length; i++)
                Console.Write("{0:X2}", D[i]);
            Console.WriteLine();
            byte[] tDH_Public = Gornerpb(DHG, DH_Secret, DHM);
            if (tDH_Public.Length != userBlock.DH_Publickey.Length)
                Console.WriteLine("Wrong DH Secret or Public values!");
            else
            {
                bool flag = true;
                for (int i = 0; i < tDH_Public.Length; i++)
                    if (tDH_Public[i] != userBlock.DH_Publickey[i])
                        flag = false;
                if (flag)
                    Console.WriteLine("DH parameters OK");
                else
                    Console.WriteLine("DH parameters error");
            }
            Console.WriteLine("Clear text:");
            byte[] data = { 1, 2, 3, 4 };
            for (int i = 0; i < data.Length; i++)
                Console.Write("{0:X2}", data[i]);
            Console.WriteLine();
            byte[] wtf = Gornerpb(data, Reverse(E), Reverse(M));
            Console.WriteLine("Cipher text:");
            for (int i = 0; i < wtf.Length; i++)
                Console.Write("{0:X2}", wtf[i]);
            Console.WriteLine();
            Console.WriteLine("Decrypted text:");
            byte[] wtfd = Gornerpb(wtf, Reverse(D), Reverse(M));
            for (int i = 0; i < wtfd.Length; i++)
                Console.Write("{0:X2}", wtfd[i]);
            Console.WriteLine();
        }
        static void CreateNewChainTest(string chainName)
        {
            Chain chain = new Chain();
            chain.AddBlock("Krevedko", Block.block_type.BType_Init, "Password");
            chain.AddBlock("@Rus", Block.block_type.BType_Init, "RusPassword");
            chain.AddBlock("SpyHunter", Block.block_type.BType_Init, "SpyPassword");
            chain.Print();
            XmlSerializer formatter = new XmlSerializer(typeof(Chain));
            using (FileStream fs = new FileStream(chainName + ".xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, chain);
            }
        }
        public class Connection
        {
            public byte[] UserId;
            public byte[] DHKey;
            public byte[] x;
            public byte[] rxkey;
            public byte[] txkey;
            public Connection()
            {
                UserId = new byte[1];
                DHKey = new byte[1];
                x = new byte[1];
            }
            public Connection(Chain c, Block bb, byte[] a)
            {
                //Block bb = c.blockChain[numb];
                UserId = bb.id_hash;

                /*Console.WriteLine("bb.DH_Publickey:");
                for (int i = 0; i < bb.DH_Publickey.Length; i++)
                    Console.Write("{0:X2}", bb.DH_Publickey[i]);
                Console.WriteLine();
                Console.WriteLine("a:");
                for (int i = 0; i < a.Length; i++)
                    Console.Write("{0:X2}", a[i]);
                Console.WriteLine();
                Console.WriteLine("c.DHM:");
                for (int i = 0; i < c.DHM.Length; i++)
                    Console.Write("{0:X2}", c.DHM[i]);
                Console.WriteLine();*/

                DHKey = Gornerpb(bb.DH_Publickey, a, c.DHM);
                byte[] tx = new byte[64];
                r.NextBytes(tx);            // rndom seed for keys
                Aes aes = Aes.Create();
                byte[] aeskey = new byte[aes.KeySize / 8];
                for (int i = 0; i < aeskey.Length; i++)
                    aeskey[i] = DHKey[i]
; RijndaelManaged aesAlg = new RijndaelManaged
{
    Key = aeskey,
    Mode = CipherMode.ECB,
    Padding = PaddingMode.None,
    KeySize = 128,
    BlockSize = 128,
    IV = new byte[16]
};
                var encryptor = aes.CreateEncryptor();
                x = encryptor.TransformFinalBlock(tx, 0, tx.Length);    // encryption for more randoom seed
                tx = encryptor.TransformFinalBlock(x, 0, x.Length);     // getting keys from seed
                for (int i = 0; i < 32; i++)
                    rxkey[i] = tx[i];
                for (int i = 32; i < 64; i++)
                    txkey[i] = tx[i];
            }
            public ulong DHKeyMAC()
            {
                ulong m = 0;
                if (DHKey == null)
                    return m;
                for (int i = 0; i < 16; i++)
                {
                    m ^= (ulong)DHKey[DHKey.Length - 1 - i] << (8 * (i & 7));
                }
                return m;
            }
        }
    }
}
