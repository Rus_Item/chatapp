﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Messaging;
using System.IO;

namespace Messenger
{
    public partial class Sendfrm : MaterialForm
    {
        public string Senduser;
        public string Receivuser;
        public List<string> Uname;
        
        public Sendfrm()
        {
            InitializeComponent();
            MaterialSkinManager manager = MaterialSkinManager.Instance;
            manager.AddFormToManage(this);
            manager.Theme = MaterialSkinManager.Themes.DARK;
            manager.ColorScheme = new ColorScheme(Primary.Blue400, Primary.Blue500, Primary.Blue500, Accent.LightBlue200, TextShade.BLACK);
        }

        public void SendMessage(MessageQueue queue, string username)
        {
           if (!MessageQueue.Exists(queue.Path))
               MessageQueue.Create(queue.Path);
           System.Messaging.Message mymessage = new System.Messaging.Message();
           mymessage.Body = textBox2.Text;
           queue.Send(mymessage);
           textBox1.Text = "TO " + username + ":" + textBox2.Text;  
        }
        public void ReceiveMessage(MessageQueue queue, string username)
        {
            System.Messaging.Message mymessage = new System.Messaging.Message();
            mymessage = queue.Receive(new TimeSpan(0, 0, 5));
            mymessage.Formatter = new XmlMessageFormatter(new string[] { "System.String,mscorlib" });
            string msg = mymessage.Body.ToString();
            textBox1.Text = "From " + username + ": " + msg;
        }
        private void lollipopButton1_Click(object sender, EventArgs e)
        {
            MessageQueue messageQueue = new MessageQueue();
            messageQueue.Path = @".\private$\" + Receivuser;
            SendMessage(messageQueue, Receivuser);
            //Task.Delay(1000);
            //ReceiveMessage(Senduser);
        }
    }
}
