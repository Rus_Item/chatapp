﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using MaterialSkin.Controls;
using MaterialSkin;
using MyLib;

namespace Messenger
{
    public partial class ucRegister : MaterialForm
    {
        public ucRegister()
        {
            InitializeComponent();
            MaterialSkinManager manager = MaterialSkinManager.Instance;
            manager.AddFormToManage(this);
            manager.Theme = MaterialSkinManager.Themes.DARK;
            manager.ColorScheme = new ColorScheme(Primary.Blue400, Primary.Blue500, Primary.Blue500, Accent.LightBlue200, TextShade.BLACK);
        }

        private void lollipopButton1_Click(object sender, EventArgs e)
        {
            string uid = "";
            string pwd = "";

            if (lollipopTextBox1.Text != "")
                uid = lollipopTextBox1.Text;
            else
                MessageBox.Show("Please enter user name!");
            if (lollipopTextBox2.Text != "")
                pwd = lollipopTextBox2.Text;
            else
                MessageBox.Show("Please enter user password!");

            MyLib.MyLib.Chain chain = null;
            MyLib.MyLib.Block b;
            MyLib.MyLib.User user = null;
            chain = new MyLib.MyLib.Chain();
            
            chain.AddBlock(uid, MyLib.MyLib.Block.block_type.BType_Init, pwd);
            XmlSerializer formatter = new XmlSerializer(typeof(MyLib.MyLib.Chain));
            using (FileStream fs = new FileStream("mychain" + ".xml", FileMode.Append))
            {
                formatter.Serialize(fs, chain);
            }
            MessageBox.Show("User created, chain saved!");
        }
    }
}
