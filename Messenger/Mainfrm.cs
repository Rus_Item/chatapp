﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messenger
{
    public partial class Mainfrm : MaterialForm
    {
        private static Mainfrm _instance;

        public static Mainfrm Instace 
        {
            get 
            {
                if (_instance == null)
                    _instance = new Mainfrm();
                return _instance;
            }
        }
        public Mainfrm()
        {
            InitializeComponent();
            MaterialSkinManager manager = MaterialSkinManager.Instance;
            manager.AddFormToManage(this);
            manager.Theme = MaterialSkinManager.Themes.DARK;
            manager.ColorScheme = new ColorScheme(Primary.Blue400, Primary.Blue500, Primary.Blue500, Accent.LightBlue200, TextShade.BLACK);
        }

        public Panel Content
        {
            get { return MainContainer; }
            set { MainContainer = value; }
        }
        private void Mainfrm_Load(object sender, EventArgs e)
        {
            _instance = this;
            MainContainer.Controls.Add(new ucLogin() { Dock=DockStyle.Fill});
        }
    }
}
