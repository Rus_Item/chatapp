﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messenger
{
    public partial class UcMain : UserControl
    {
        public UcMain()
        {
            InitializeComponent();
        }

        private void listOnline_Click(object sender, EventArgs e)
        {
            if (listOnline.SelectedItem != null)
            {
                Sendfrm frm = new Sendfrm();
                frm.Show();
                frm.Receivuser = listOnline.SelectedItem.ToString();
            }
            else
                MessageBox.Show("User not selected!");
        }
    }
}
